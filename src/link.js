import React from 'react'
import { push as pushAction, replace as replaceAction } from './actions'
import PropTypes from 'prop-types'
import UrlPattern from 'url-pattern'
import Url from 'url'
import withRouter from './withRouter'

const LEFT_MOUSE_BUTTON = 0
const isNotLeftClick = e => e.button && e.button !== LEFT_MOUSE_BUTTON
const hasModifier = e => Boolean(e.shiftKey || e.altKey || e.metaKey || e.ctrlKey)
const shouldIgnoreClick = ({ event, target }) => hasModifier(event) || isNotLeftClick(event) || event.defaultPrevented || target

const createClickHandler = ({ onClick, replaceState, dispatch, href, target }) => event => {
  if (onClick) {
    onClick(event)
  }

  if (shouldIgnoreClick({ event, target })) {
    return
  }

  event.preventDefault()

  let action = replaceState
    ? replaceAction
    : pushAction

  dispatch(action(href))
}

const parseHref = ({ href, query }) => {
  if (href instanceof UrlPattern) {
    href = href.stringify(query || {})
  }

  let url = Url.parse(href, true)

  if (query) {
    url.query = Object.assign({}, url.query, query)
  }

  return Url.format(url)
}

const matchesLocation = (href, location) => {
  let url = Url.parse(href)
  return url.pathname === location.pathname
}

const combineClassNames = (props, activeProps) => {
  if (typeof (props.className) === 'string' && typeof (props.className) === 'string') {
    return props.className + ' ' + activeProps.className
  }
  return props.className || activeProps.className
}

const Link = ({
  replaceState,
  children,
  onClick,
  as: Component,
  location,
  activeProps,
  dispatch,
  target,
  ...restProps
}) => {
  let href = parseHref(restProps)

  const isActive = matchesLocation(href, location)

  let className = isActive
    ? combineClassNames(restProps, activeProps)
    : restProps.className

  return React.createElement(
    Component,
    {
      ...restProps,
      ...(isActive ? activeProps : {}),
      href,
      target,
      onClick: createClickHandler({ onClick, replaceState, dispatch, href, target  }),
      className
    },
    children
  )
}

Link.propTypes = {
  href: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(UrlPattern)
  ]).isRequired,
  replaceState: PropTypes.bool,
  target: PropTypes.string,
  onClick: PropTypes.func,
  query: PropTypes.object,
  as: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.string
  ]),
  children: PropTypes.node,
  location: PropTypes.object,
  activeProps: PropTypes.object,
  dispatch: PropTypes.func
}
Link.defaultProps = {
  as: 'a',
  activeProps: {}
}

export default withRouter(Link)
