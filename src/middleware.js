import * as Actions from './actions'
import { parseLocation } from './helpers'
import Url from 'url'

const navigate = (history, action) => {
  switch (action.type) {
    case String(Actions.push):
      history.push(action.payload)
      break
    case String(Actions.replace):
      history.replace(action.payload)
      break
    case String(Actions.go):
      history.go(action.payload)
      break
    case String(Actions.back):
      history.goBack()
      break
    case String(Actions.forward):
      history.goForward()
      break
    case String(Actions.initializeLocation):
      history.replace(Url.format(action.location))
      break
    default:
      break
  }
}

export const createMiddleware = (history, patterns) => (store) => {
  history.listen((location) => {
    let payload = parseLocation(location, patterns)
    store.dispatch(Actions.locationChanged(location, payload))
  })

  return (next) => (action) => {
    const originalDispatch = next(action)

    navigate(history, action)

    return originalDispatch
  }
}

export const createSSRMiddleware = (history, patterns) => (store) => {
  return (next) => (action) => {
    const originalDispatch = next(action)

    let location = history.location

    navigate(history, action)

    if (location !== history.location) {
      let payload = parseLocation(history.location, patterns)
      store.dispatch(Actions.locationChanged(history.location, payload))
    }

    return originalDispatch
  }
}
