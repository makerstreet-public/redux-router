import { connect } from 'react-redux'
import reducer from './reducer'

export default (component) => connect((state) => ({
  location: (typeof (state.get) === 'function'
    ? state.get(String(reducer))
    : state[String(reducer)]).location
}))(component)
