import { createActionsDomain } from '@touchtribe/redux-helpers'
import UrlPattern from 'url-pattern'
import Url from 'url'

const createActions = createActionsDomain('redux-router')

const payloadFromAction = (href, query = {}) => {
  if (href instanceof UrlPattern) {
    href = href.stringify(query)
  }
  const url = Url.parse(href, true)
  url.query = Object.assign({}, url.query, query)
  return { payload: Url.format(url) }
}

export const [
  push,
  replace,
  go,
  locationChanged,
  initializeLocation,
  back,
  forward
] = createActions('route', {
  push: payloadFromAction,
  replace: payloadFromAction,
  go: payloadFromAction,
  locationChanged: (location, payload) => ({ location, payload }),
  initializeLocation: (location) => { return { location } },
  back: () => {},
  forward: () => {}
})
