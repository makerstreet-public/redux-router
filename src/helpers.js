import UrlPattern from 'url-pattern'
import QueryString from 'query-string'

const toString = function () { return this.stringify() }
export const createPattern = (pattern) => Object.assign(new UrlPattern(pattern), { toString })

export const parseLocation = (location, patterns) => {
  patterns = Array.isArray(patterns)
    ? patterns
    : Object.keys(patterns).map(key => patterns[key])

  const pattern = patterns.find(pattern => pattern.match(location.pathname))

  return {
    location,
    pattern,
    matches: pattern ? pattern.match(location.pathname) : {},
    params: QueryString.parse(location.search),
    hash: QueryString.parse(location.hash)
  }
}
