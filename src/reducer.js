import * as Actions from './actions'
import { createReducer } from '@touchtribe/redux-helpers'

const initialState = {
  location: {},
  pattern: undefined,
  params: {},
  matches: {}
}

export default createReducer('router', {
  [Actions.locationChanged]: (state, { payload }) => payload || state
}, initialState)
