import React from 'react'
import PropTypes from 'prop-types'
import UrlPattern from 'url-pattern'
import withRouter from './withRouter'

const Route = ({ pattern, location, component, children, ...props }) => {
  if (typeof pattern === 'string') {
    pattern = new UrlPattern(pattern)
  }

  const match = pattern.match(location.pathname)

  props = { ...props, location, pattern, match }

  if (component) {
    return match
      ? React.createElement(component, props)
      : null
  }

  if (typeof children === 'function') {
    return children(props)
  }

  if (match && children && React.Children.count(children) > 0) {
    return children
  }

  return null
}
Route.propTypes = {
  pattern: PropTypes.oneOfType([
    PropTypes.instanceOf(UrlPattern),
    PropTypes.string
  ]).isRequired,
  location: PropTypes.object,
  component: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node])
}

export default withRouter(Route)
